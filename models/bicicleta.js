var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number]
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) { 
    return new this({   
        code: code,
        color: color,
        modelo: modelo, 
        ubicacion: ubicacion
    })
}

bicicletaSchema.methods.toString = () => {
    return `Bicileta\ncode: ${this.code}\ncolor: ${this.color}`;
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema)