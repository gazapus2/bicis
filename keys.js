var MONGO_URI = '';

switch(process.env.NODE_ENV.replace(/\W/g, '')){
    case 'test': 
        MONGO_URI = "mongodb://localhost/testdb"; 
        break;
    case 'development': 
        MONGO_URI = "mongodb://localhost/bicicletas_red"; 
        break;
}

exports.MONGO_URI = MONGO_URI;