var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = async function (req, res) {
    try {
        let bicis = await Bicicleta.find({});
        res.render('bicicletas/index', { bicis: bicis })
    } catch (e) {
        res.status(500).render('error')
    }
}

exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res) {
    var bici = Bicicleta.createInstance(
        req.body.code,
        req.body.color,
        req.body.modelo,
        [req.body.latitud, req.body.longitud]
    )
    bici.save((err, biciSaved) => {
        if (err) res.status(500).render('error')
        res.redirect('/bicicletas')
    })
}

exports.bicicleta_delete_post = function (req, res) {
    Bicicleta.deleteOne({ code: req.params.code }, (err) => {
        if (err) return res.status(400).render('error', { error: err })
        res.redirect('/bicicletas')
    })
}

exports.bicicleta_update_get = async function (req, res) {
    let bici = await Bicicleta.findOne({ code: req.params.code });
    res.render('bicicletas/update', { bici });
}

exports.bicicleta_update_post = function (req, res) {
    let bici = {
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.latitud, req.body.longitud]
    }
    Bicicleta.findOneAndUpdate({code: req.body.code}, bici, (err, doc) => {
        if (err) return res.status(400).render('error', { error: err })
        res.redirect('/bicicletas');
    })
}