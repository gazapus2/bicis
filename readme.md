# Segunda entrega del curso Desarrollo del lado del Servidor
## PROYECTO RED DE BICICLETAS
Se trata de un sencillo proyecto web centrado en el uso del framework express

![Preview](https://iili.io/2nTpFs.jpg)

#### REQUISITOS
*   [nodejs](https://nodejs.dev/) 
*   [postman](postman.com) 
*   [mongodb](mongodb.com)

#### INSTRUCCIONES

Para usar el proyecto es necesario instalar las dependencias mediante el comando:
```
npm install 
```

Para correr el proyecto en el puerto 3000 :
```
npm run devstart
```

Para correr todos los test:
```
npm test
```

Para correr en el navegador entrar a:

* [localhost:3000](http://localhost:3000/)

La base de datos de prueba es `testdb`

La base de datos de producción es `bicicletas_red`

#### API

Los endpoints de la API son:

* Bicicletas 

    * http://localhost:3000/api/bicicletas (GET)

    * http://localhost:3000/api/bicicletas/create  (POST) :
        * _body: { code: Number, modelo: String, color: String, ubicacion: [Number, Number]}_

    * http://localhost:3000/api/bicicletas/delete (POST):
        * _body { code: Number}_

    * http://localhost:3000/api/bicicletas/update (POST):
        * _body: { code: Number, modelo: String, color: String, ubicacion: [Number, Number]}_

* Usuarios 

    * http://localhost:3000/api/usuarios (GET)

    * http://localhost:3000/api/usuarios/get/:id_usuario  (GET)

    * http://localhost:3000/api/usuarios/create (POST):
        * _body: { name: String }_

    * http://localhost:3000/api/usuarios/delete/:id_usuario (POST)

    * http://localhost:3000/api/usuarios/update (POST):
        * _body: { id: String, name: String }_

* Reservas 

    * http://localhost:3000/api/reservas (GET)

    * http://localhost:3000/api/reservas/getById/:id_reserva (GET) 

    * http://localhost:3000/api/reservas/getByUser/:id_usuario (GET)

    * http://localhost:3000/api/reservas/create (POST):
        * _body: { usuario: String, bicicleta: String, desde: Date, hasta: Date }_

    * http://localhost:3000/api/reservas/update (POST):
        * _body: { id: String, usuario: String, bicicleta: String, desde: Date, hasta: Date }_

    * http://localhost:3000/api/reservas/delete/:id_reserva (POST)