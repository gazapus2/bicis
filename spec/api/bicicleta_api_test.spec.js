var got = require('got');
require('../../bin/www');  // Al traer el server ya se ejecuta
var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');
const keys = require('../../keys');

mongoose.set('useFindAndModify', false);    // Evita mensaje de deprecación

const URL_BASE = 'http://localhost:3000/api/bicicletas';

describe('API TESTING', () => {

    beforeAll((done) => {
        mongoose.connect(keys.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'falló mongoose'));
        db.once('open', function () {
            console.log('mongoose conectado');
            done();
        });
    });
    
    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log('error al borrar todos');
            done();
        })
    });
    
    afterAll(() => {
        mongoose.connection.close();
    })

    it(' GET ALL', async (done) => {
        const response = await got(URL_BASE);
        expect(response.statusCode).toBe(200);
        done();
    })

    it('CREATE', (done) => {
        let bodyRequest = {code: 99, color: 'red', modelo: 'xx', ubicacion: [10,10]}
        got.post(URL_BASE + '/create', { json: bodyRequest })
            .then(response => {
                expect(response.statusCode).toBe(200);
            })
            .catch((error) => {
                expect(500).toBe(200);
            })
            .finally(() => {
                done();
            })
    })

    it(' DELETE', async (done) => {
        let newBiciBody = {code: 66, color: 'red', modelo: 'xx', ubicacion: [10,10]}
        try {
            await got.post(URL_BASE + '/create', { json: newBiciBody });
            let response = await got.post(URL_BASE + '/delete', { json: {code: 66} });
            expect(response.statusCode).toBe(204);
        } 
        catch  (error) {
            expect(false).toBe(true);
        }
        finally {
            done();
        }
    })

    it(' UPDATE', async (done) => {
        let newBiciBody = {code: 77, color: 'red', modelo: 'xx', ubicacion: [10,10]}
        let updateBiciBody = {code: 77, color: 'orange', modelo: 'urban', ubicacion: [10,10]}
        try {
            await got.post(URL_BASE + '/create', { json: newBiciBody });
            let response = await got.post(URL_BASE + '/update', { 
                json: updateBiciBody,
                responseType: 'json'
            });
            expect(response.statusCode).toBe(200);
            expect(response.body.color).toBe('orange');
        } 
        catch  (error) {
            expect(500).toBe(200);
        }
        finally {
            done();
        }
    });
});