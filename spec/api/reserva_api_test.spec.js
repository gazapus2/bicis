var got = require('got');
require('../../bin/www');
var Reserva = require('../../models/reserva');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var mongoose = require('mongoose');
const keys = require('../../keys');

mongoose.set('useFindAndModify', false);

const URL_BASE = 'http://localhost:3000/api/reservas';

describe('API USUARIO TESTING', () => {

    beforeAll((done) => {
        mongoose.connect(keys.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'falló mongoose'));
        db.once('open', function () {
            console.log('mongoose conectado');
            done();
        });
    });

    afterEach((done) => {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log('error al borrar todos');
            done();
        })
    });

    afterAll(() => {
        mongoose.connection.close();
    })

    it(' GET ALL RESERVAS', async (done) => {
        const response = await got(URL_BASE);
        expect(response.statusCode).toBe(200);
        done();
    })

    it(' CREAR Y TRAER UNA RESERVA', async (done) => {
        let newUser = new Usuario({ name: 'Pepe3' });
        let newBici = new Bicicleta({ code: 1, color: 'red', modelo: 'urban', ubicacion: [1, 1] });
        try {
            await got.post('http://localhost:3000/api/usuarios/create', { json: newUser });
            await got.post('http://localhost:3000/api/bicicletas/create', { json: newBici });
            let newReserva = new Reserva({
                usuario: newUser._id,
                bicicleta: newBici._id,
                desde: new Date(),
                hasta: new Date(2021, 1, 1)
            })
            let responseCreate = await got.post(URL_BASE + '/create', { json: newReserva });
            let responseGet = await got(URL_BASE + `/getById/${newReserva._id}`);
            expect(responseGet.statusCode).toBe(200);
            expect(responseCreate.statusCode).toBe(200);

        }
        catch (error) {
            expect(false).toBe(true);
        }
        finally {
            done();
        }
    })


    it(' ELIMINAR RESERVA', async (done) => {
        let newUser = new Usuario({ name: 'Pepe3' });
        let newBici = new Bicicleta({ code: 1, color: 'red', modelo: 'urban', ubicacion: [1, 1] });
        try {
            await got.post('http://localhost:3000/api/usuarios/create', { json: newUser });
            await got.post('http://localhost:3000/api/bicicletas/create', { json: newBici });
            let newReserva = new Reserva({
                usuario: newUser._id,
                bicicleta: newBici._id,
                desde: new Date(),
                hasta: new Date(2021, 1, 1)
            })
            await got.post(URL_BASE + '/create', { json: newReserva });
            let response = await got.post(URL_BASE + `/delete/${newReserva._id}`);
            expect(response.statusCode).toBe(200);
        }
        catch (error) {
            expect(false).toBe(true);
        }
        finally {
            done();
        }
    })
});