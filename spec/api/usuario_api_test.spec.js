var got = require('got');
require('../../bin/www');  
var Usuario = require('../../models/usuario');
var mongoose = require('mongoose');
const keys = require('../../keys');

mongoose.set('useFindAndModify', false);    

const URL_BASE = 'http://localhost:3000/api/usuarios';

describe('API USUARIO TESTING', () => {

    beforeAll((done) => {
        mongoose.connect(keys.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'falló mongoose'));
        db.once('open', function () {
            console.log('mongoose conectado');
            done();
        });
    });
    
    afterEach((done) => {
        Usuario.deleteMany({}, (err, success) => {
            if (err) console.log('error al borrar todos');
            done();
        })
    });
    
    afterAll(() => {
        mongoose.connection.close();
    })

    it(' GET ALL USERS', async (done) => {
        const response = await got(URL_BASE);
        expect(response.statusCode).toBe(200);
        done();
    })

    it(' GET AN USER', async (done) => {
        let newUser = new Usuario({name: 'Pepe3'});
        try {
            await got.post(URL_BASE + '/create', { json: newUser });
            let response = await got(URL_BASE + `/get/${newUser._id}`);
            expect(response.statusCode).toBe(200);
        } 
        catch  (error) {
            expect(false).toBe(true);
        }
        finally {
            done();
        }
    })

    it('CREATE USER', (done) => {
        got.post(URL_BASE + '/create', { json: {name: 'Julio Roca'} })
            .then(response => {
                expect(response.statusCode).toBe(200);
            })
            .catch((error) => {
                expect(500).toBe(200);
            })
            .finally(() => {
                done();
            })
    })

    it(' DELETE USER', async (done) => {
        let newUser = new Usuario({name: 'Pepe'});
        try {
            await got.post(URL_BASE + '/create', { json: newUser });
            let response = await got.post(URL_BASE + `/delete/${newUser._id}`);
            expect(response.statusCode).toBe(200);
        } 
        catch  (error) {
            expect(false).toBe(true);
        }
        finally {
            done();
        }
    })

    it(' UPDATE USER', async (done) => {
        let newUser = new Usuario({name: 'Pepe2'});
        try {
            await got.post(URL_BASE + '/create', { json: newUser });
            let response = await got.post(URL_BASE + `/update`, {json: {name: 'Pepe3'}});
            expect(response.statusCode).toBe(200);
        } 
        catch  (error) {
            expect(false).toBe(true);
        }
        finally {
            done();
        }
    });
});