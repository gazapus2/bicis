var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');

const MONGO_URI = "mongodb://localhost/testdb";

describe('TESTING USUARIO', () => {

    beforeAll((done) => {
        mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'falló conexion a la base de datos'));
        db.once('open', function () {
            console.log('mongoose conectado');
            done();
        });
    });

    afterEach((done) => {
        Usuario.deleteMany({}, (err, success) => {
            if (err) console.log('error al borrar todos');
            done();
        })
    });

    afterAll(() => {
        mongoose.connection.close();
    })

    it("Crea una instancia de usuario y lo trae de la DB", async () => {
        var user = new Usuario({ name: 'Pepe' })
        await user.save();
        let userSaved = await Usuario.findOne({ name: user.name });
        expect(userSaved.name).toBe(user.name);
    })

    it("usuario.Reservar()", async (done) => {
        var usuario = new Usuario({ name: 'Roberto' })
        await usuario.save();
        let bicicleta = new Bicicleta({ code: 15, color: 'violet', model: 'xfp', ubicacion: [1, 1] });
        await bicicleta.save();
        let reserva = await usuario.reservar(
            bicicleta._id,
            new Date(),
            new Date(2021, 1, 1)
        );
        await reserva.populate('bicicleta').populate('usuario').execPopulate();
        expect(reserva.usuario.name).toEqual(usuario.name);
        expect(reserva.bicicleta.color).toEqual(bicicleta.color);
        done();
    })
})