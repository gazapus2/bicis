var express = require('express');
var router = express.Router();
var reservaController = require('../../controllers/api/reservaControllerAPI');

router.get('/', reservaController.reservas_list);
router.get('/getById/:id', reservaController.get_reserva);
router.get('/getByUser/:userId', reservaController.get_reserva_by_user);
router.post('/create', reservaController.create_reserva);
router.post('/delete/:id', reservaController.delete_reserva);
router.post('/delete_all', reservaController.delete_all);
router.post('/update', reservaController.update);

module.exports = router;