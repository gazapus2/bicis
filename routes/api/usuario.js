var express = require('express');
var router = express.Router();
var usuarioController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioController.usuarios_list);
router.get('/get/:id', usuarioController.get_usuario);
router.post('/create', usuarioController.create_usuario);
router.post('/delete/:id', usuarioController.delete_usuario);
router.post('/clear', usuarioController.delete_all);
router.post('/update', usuarioController.update);

module.exports = router;