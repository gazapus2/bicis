var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicletaController');

router.get('/', bicicletaController.bicicleta_list);
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.bicicleta_create_post);
router.post('/delete/:code', bicicletaController.bicicleta_delete_post);
router.get('/update/:code', bicicletaController.bicicleta_update_get);
router.post('/update/:code', bicicletaController.bicicleta_update_post);

module.exports = router;